//
//  SearchProductsUseCase.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//
import Foundation

protocol iSearchProductUseCase {
	func execute(request: String,
				 responseHandler: @escaping((_ data: Results?) -> Void),
				 errorHandler: @escaping((_ error: NetworkError) -> Void) )
}

class SearchProductsUseCase: iSearchProductUseCase {
	private var repository: iFetchProductsRepository? = ServiceLocator.instance.get()
	
	func execute(request: String,
				 responseHandler: @escaping ((Results?) -> Void),
				 errorHandler: @escaping ((NetworkError) -> Void)) {
		
		repository?.fetchProducts(by: request, response: { data in
			responseHandler(data)
		}, errorHndr: { error in
			errorHandler(error as! NetworkError)
		})
	}
}
