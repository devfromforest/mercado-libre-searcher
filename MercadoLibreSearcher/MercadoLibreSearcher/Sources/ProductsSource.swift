//
//  ProductsSource.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 11/09/21.
//

import UIKit

class ProductsSource<D: DataIndexable, C: ConfigurableCell>: NSObject, UITableViewDelegate, UITableViewDataSource where D.ItemType == C.ItemType {
	
	private var _data: D!
	private var _delegate: ProductsSourceDelegate!
	
	var data: D{
		get{return _data}
		set{_data = newValue}
	}
	
	var delegate: ProductsSourceDelegate{
		get{ return _delegate }
		set{ _delegate = newValue}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableView.automaticDimension
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.getCount()
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let identifier = C.reuseIdentifierForIndexPath(indexPath: indexPath)
		guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? C.CellType else {return UITableViewCell()}
		let item = data.getObjectAtIndex(indexPath: indexPath)
		C.configureCell(indexPath: indexPath, item: item, cell: cell)
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let item = data.getObjectAtIndex(indexPath: indexPath)
		_delegate.itemSelected(item: item as! Product)
	}
}
