//
//  DataIndexable.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//
import Foundation

protocol DataIndexable {
	associatedtype ItemType
	func getObjectAtIndex(indexPath: IndexPath) -> ItemType
	func getCount() -> Int
}

extension Array: DataIndexable{

	typealias ItemType = Element
	
	func getObjectAtIndex(indexPath: IndexPath) -> Element {
		return self[indexPath.item]
	}
	
	func getCount() -> Int {
		return self.count
	}
}
