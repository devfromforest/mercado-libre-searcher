//
//  BaseRepository.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//
import Alamofire
import SwiftyJSON
import RxSwift

enum NetworkError: Error {
	case noInternet
	case apiFailure
	case invalidResponse
	case decodingError
}

protocol IRepositorySettings {
	var BaseURL: String {get set}
	var fullUrl: String {get set}
	var requestParams: [String: Any] {get set}
	var headerParams: HTTPHeaders {get set}
	var REQUEST_TIMEOUT: Double {get set}
}

class BaseRepository: IRepositorySettings {
	
	var BaseURL: String = "https://api.mercadolibre.com/sites/"
	var fullUrl: String = ""
	var requestParams: [String : Any] = [:]
	var headerParams: HTTPHeaders = []
	var REQUEST_TIMEOUT: Double = 20
	
	var urlHelper: UrlHelper!
	typealias responseCompletion = ( _ data: JSON) -> Void
	typealias errorCompletion = ( _ error: NetworkError) -> Void
	let disposeBag = DisposeBag()
	
	lazy var sessionManager: Session = {
		let configuration = URLSessionConfiguration.af.default
		configuration.timeoutIntervalForRequest = REQUEST_TIMEOUT
		return Session(configuration: configuration)
	}()
	
	init() {
		urlHelper = UrlHelper()
	}

	func consumeService(method: HTTPMethod,
						responseCompletion: @escaping(responseCompletion),
						errorCompletion: @escaping(errorCompletion)){
		
		var dataResponse: JSON!
		
		Observable<JSON>.create { [self] observer in
			sessionManager
				.request(fullUrl, method: method, parameters: requestParams, headers: headerParams)
				.validate(statusCode: 200..<300)
				.validate(contentType: ["application/json"])
				.responseJSON { response in
					switch (response.result) {
					case .success( _):
						guard let data = response.data else {return}
						do{
							let json = try JSON(data: data)
							observer.onNext(json)
							observer.onCompleted()
						} catch let error {
							print(String(describing: error.localizedDescription))
							observer.onError(NetworkError.invalidResponse)
						}
					case .failure(_):
						observer.onError(NetworkError.apiFailure)
					}
				}
			
			return Disposables.create()
		}
		.observe(on: SerialDispatchQueueScheduler(qos: .background))
		.subscribe(on: MainScheduler.instance)
		.subscribe { data in
			dataResponse = data
		} onError: { error in
			errorCompletion(error as! NetworkError)
		} onCompleted: {
			responseCompletion(dataResponse)
		}.disposed(by: disposeBag)
	}

}
