//
//  MatchProducts.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 11/09/21.
//
import SwiftyJSON

class MatchProducts: AnalyzeAndMatchResult {
	func matchData(json: JSON) -> Any? {
		var products = [Product]()
		let siteId = json["site_id"].stringValue
		let query = json["query"].stringValue
		
		let list: Array<JSON> = json["results"].arrayValue
		
		for item in list {
			let id = item["id"].stringValue
			let title = item["title"].stringValue
			let thumbnail = item["thumbnail_id"].stringValue
			let price = item["price"].int64
			let priceFormatted = String(price!).currencyFormat(value: Double(price!))
			products.append(Product(id: id, title: title, thumbnail: thumbnail, price: priceFormatted))
		}
		
		let results = Results(site_id: siteId, query: query, results: products)
		return results
	}

}
