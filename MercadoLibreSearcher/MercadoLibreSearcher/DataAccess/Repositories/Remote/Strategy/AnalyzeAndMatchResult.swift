//
//  JsonResultStrategy.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 11/09/21.
//

import SwiftyJSON

protocol AnalyzeAndMatchResult {
	func matchData(json: JSON) -> Any?
}
