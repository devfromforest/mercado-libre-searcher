//
//  ColorHelper.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//
import UIKit

enum AppColors : String{
	case primary = "default_yellow"
	case blankAccent = "blank_accent"
	case darkAccent = "dark_accent"
}

class ColorHelper {
	func getColor(color: AppColors) -> UIColor{
		return UIColor(named: color.rawValue)!
	}
}
