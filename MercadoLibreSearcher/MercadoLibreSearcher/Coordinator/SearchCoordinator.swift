//
//  SearchCoordinator.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class SearchCoordinator: Coordinator {
	
	var childCoordinators = [Coordinator]()
	
	var navigationController: UINavigationController
	weak var parentCoordinator: MainCoordinator?
	var colorHelper: ColorHelper!
	
	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
		colorHelper = ColorHelper()
		setBarStyle()
	}
	
	private func setBarStyle(){
		let navigationBar = navigationController.navigationBar
		navigationBar.barTintColor = colorHelper.getColor(color: .primary)
		navigationBar.titleTextAttributes = [.foregroundColor:  colorHelper.getColor(color: .darkAccent)]
		navigationBar.backItem?.title = Strings.back_button.localized
		navigationBar.tintColor = colorHelper.getColor(color: .darkAccent)
	}
	
	func start() {
		let vc = SearchVc()
		vc.coordinator = self
		vc.modalPresentationStyle = .fullScreen
		navigationController.pushViewController(vc, animated: true)
	}
	
	func showResultSearch(text: String){
		let vc = ResultsVc()
		vc.coordinator = self
		vc.title = text
		vc.textToSearch = text
		vc.modalPresentationStyle = .fullScreen
		navigationController.pushViewController(vc, animated: true)
	}
	
	func showDetails(product: Product){
		let vc = DetailVc()
		vc.product = product
		vc.modalPresentationStyle = .fullScreen
		navigationController.pushViewController(vc, animated: true)
	}
}
