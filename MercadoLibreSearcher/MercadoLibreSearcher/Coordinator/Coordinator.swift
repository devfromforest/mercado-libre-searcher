//
//  Coordinator.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

protocol Coordinator {
	var childCoordinators: [Coordinator] {get set}
	var navigationController: UINavigationController {get set}
	func start()
}
