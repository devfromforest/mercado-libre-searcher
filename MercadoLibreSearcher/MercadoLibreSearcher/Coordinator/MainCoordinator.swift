//
//  MainCoordinator.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//
import UIKit

class MainCoordinator: Coordinator {
	
	var childCoordinators = [Coordinator]()
	
	var navigationController: UINavigationController
	
	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	func start() {
		let vc = SplashScreenVc()
		vc.coordinator = self
		navigationController.pushViewController(vc, animated: true)
	}
	
	func showSearch(){
		let child = SearchCoordinator(navigationController: navigationController)
		child.parentCoordinator = self
		childCoordinators.append(child)
		child.start()
	}
	
}
