//
//  Product.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//
import UIKit

struct Product: Decodable {
	var id: String
	var title: String
	var thumbnail: String
	var price: String
}
