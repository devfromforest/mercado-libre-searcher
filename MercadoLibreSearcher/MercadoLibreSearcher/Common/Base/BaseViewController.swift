//
//  BaseViewController.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 12/09/21.
//

import UIKit

class BaseViewController: UIViewController {
	
	let loadingView: LoadingView = {
		let loadingView = LoadingView(frame: .zero)
		loadingView.translatesAutoresizingMaskIntoConstraints = false
		loadingView.cornerRadius(10)
		loadingView.setBorder(with: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
		return loadingView
	}()
	
	let errorView: ErrorView = {
		let errorView = ErrorView(frame: .zero)
		errorView.translatesAutoresizingMaskIntoConstraints = false
		errorView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
		errorView.cornerRadius(10)
		return errorView
	}()
	
	func hideLoading(){
		loadingView.isHidden = true
		loadingView.stopAnimation()
	}
	
	func showLoading(){
		loadingView.isHidden = false
		loadingView.startAnimation()
	}
	
	func hideErrorView(){
		errorView.isHidden = true
	}
	
	func showErrorView(message: String){
		errorView.isHidden = false
		errorView.setMessage(errorMessage: message)
	}
}
