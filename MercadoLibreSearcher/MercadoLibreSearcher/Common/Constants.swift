//
//  Constants.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 13/09/21.
//

import Foundation

enum Strings {
	static let searcher_title = "input_search_placeholder"
	static let results_title = "items_founds"
	static let back_button = "back_button_bar_title"
	static let results_not_found = "not_found_message"
	static let not_internet_connection = "not_internet_connection"
	static let error_occurred = "error_searching"
}

enum Fonts {
	static let regular_font = "AvenirNext-Regular"
	static let bold_font = "AvenirNext-Bold"
}
