//
//  ProductCell.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 11/09/21.
//

import UIKit

class ProductCell: UITableViewCell, ConfigurableCell {

	typealias CellType = ProductCell
	typealias ItemType = Product

	private let productNameLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.font = UIFont(name: Fonts.regular_font, size: 18.0)
		lbl.textColor = .darkText
		lbl.textAlignment = .left
		lbl.numberOfLines = 0
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()
	
	private let productPriceLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.font = UIFont(name: Fonts.regular_font, size: 18.0)
		lbl.textColor = .black
		lbl.textAlignment = .left
		lbl.numberOfLines = 1
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()

	private lazy var mainView: UIView = {
		let view = UIView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(productNameLbl)
		view.addSubview(productPriceLbl)
		view.cornerRadius(8)
		view.setBorder(with: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
		return view
	}()
	
	override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	static func reuseIdentifierForIndexPath(indexPath: IndexPath) -> String {
		return "ProductCell"
	}
	
	private func setupViews(){
		contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
		addSubview(mainView)
		backgroundColor = .clear
		selectionStyle = .none
	}
	
	private func setConstraints(){
		let safeArea = self.safeAreaLayoutGuide
		NSLayoutConstraint.activate([
			mainView.topAnchor.constraint(equalTo:  safeArea.topAnchor),
			mainView.leadingAnchor.constraint(equalTo:  safeArea.leadingAnchor),
			mainView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
			mainView.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -8)
		])
		
		NSLayoutConstraint.activate([
			productNameLbl.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 6),
			productNameLbl.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 6),
			productNameLbl.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -6)
		])
		
		NSLayoutConstraint.activate([
			productPriceLbl.topAnchor.constraint(equalTo: productNameLbl.bottomAnchor, constant: 6),
			productPriceLbl.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 6),
			productPriceLbl.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: 6),
			productPriceLbl.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -6)
		])
	}
	
	static func configureCell(indexPath: IndexPath, item: ItemType, cell: ProductCell) {
		cell.productNameLbl.text = item.title
		cell.productPriceLbl.text = item.price
	}
	
}
