//
//  LoadingView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 12/09/21.
//

import UIKit

class LoadingView: UIView {
	
	private var loadingAI: UIActivityIndicatorView = {
		let indicator = UIActivityIndicatorView(style: .medium)
		indicator.hidesWhenStopped = false
		indicator.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
		indicator.color = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
		indicator.translatesAutoresizingMaskIntoConstraints = false
		return indicator
	}()
	
	private var loadingMessage: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = "Downloading..."
		lbl.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
		lbl.textAlignment = .center
		lbl.numberOfLines = 1
		lbl.font = UIFont(name: "AvenirNext-regular", size: 14.0)
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews(){
		backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
		addSubview(loadingAI)
		addSubview(loadingMessage)
	}
	
	private func setConstraints(){
		let safeArea = self.safeAreaLayoutGuide
		NSLayoutConstraint.activate([
			loadingAI.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 10),
			loadingAI.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
			loadingAI.heightAnchor.constraint(equalToConstant: 30),
			loadingAI.widthAnchor.constraint(equalToConstant: 30),
		])
		
		NSLayoutConstraint.activate([
			loadingMessage.topAnchor.constraint(equalTo: loadingAI.bottomAnchor, constant: 6),
			loadingMessage.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
			loadingMessage.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
			loadingMessage.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor)
		])
	}
	
	func bindContraints(to parent: UIView){
		parent.addSubview(self)
		NSLayoutConstraint.activate([
			self.centerYAnchor.constraint(equalTo: parent.centerYAnchor),
			self.centerXAnchor.constraint(equalTo: parent.centerXAnchor),
			self.heightAnchor.constraint(equalToConstant: 100),
			self.widthAnchor.constraint(equalToConstant: 200)
		])
	}
	
	func startAnimation(){
		loadingAI.startAnimating()
	}
	
	func stopAnimation(){
		loadingAI.stopAnimating()
	}
}
