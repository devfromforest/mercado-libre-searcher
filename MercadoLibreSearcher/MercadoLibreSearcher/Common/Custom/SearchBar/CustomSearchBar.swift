//
//  CustomSearchBar.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 8/09/21.
//
import UIKit

class CustomSearchBar: NSObject {
	private var searchController: UISearchController?
	private var searchBar: UISearchBar?
	private(set) var delegate: CustomSearchBarDelegate?
	private var _placeholder: String? = ""
	
	override init() {
		delegate = CustomSearchBarDelegate()
	}
	
	var placeholder: String {
		get{
			return _placeholder!
		}
		set{
			_placeholder = newValue
		}
	}
	
	func setupSearchBar(parent: UIViewController, resultsVc: UIViewController){
		setSearchController(resultsVc: resultsVc)
		setSearchBar(parent: parent)
	}
	
	private func setSearchController(resultsVc: UIViewController){
		searchController = UISearchController(searchResultsController: resultsVc)
		searchController?.searchResultsUpdater = delegate
		searchController?.showsSearchResultsController = true
		searchController?.hidesNavigationBarDuringPresentation = false
	}
	
	private func setSearchBar(parent: UIViewController){
		searchBar = searchController!.searchBar
		searchBar!.delegate = delegate
		searchBar!.sizeToFit()
		searchBar!.backgroundImage = UIImage()
		searchBar!.placeholder = _placeholder
		parent.definesPresentationContext = true
	}
	
	func getSearchBar() -> UISearchBar{
		return searchBar!
	}
}
