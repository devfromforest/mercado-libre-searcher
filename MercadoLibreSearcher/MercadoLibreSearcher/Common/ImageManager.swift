//
//  ImageManager.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 14/09/21.
//

import Foundation
import Alamofire
import AlamofireImage

class ImageManager{
	
	private let thumbNailDomain: String = "https://http2.mlstatic.com/"
	private var finalUrl: String!
	private var thumbnailID: String!
	
	func thumbnailID(id: String) -> Self{
		self.thumbnailID = id
		return self
	}
	
	func downloadImage(completion: @escaping(_ image: Image) -> ()){
		let downloader = ImageDownloader()
	
		let urlRequest = URLRequest(url: URL(string: matchUrl())!)

		downloader.download(urlRequest, completion:  { response in
			if case .success(let image) = response.result {
				completion(image)
			}
		})
	}
	
	private func matchUrl() -> String{
		let urlMatched = "\(thumbNailDomain)D_\(thumbnailID!)-I.jpg"
		return urlMatched
	}
}
