//
//  UIView+Extension.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 12/09/21.
//

import UIKit

extension UIView{
	func cornerRadius(_ radius: CGFloat?){
		self.layer.cornerRadius = radius!
		if let img = self as? UIImageView {
			img.layer.masksToBounds = true
		}
	}
	func setBorder(with color: UIColor){
		self.layer.borderWidth = 1
		self.layer.borderColor = color.cgColor
	}
}
