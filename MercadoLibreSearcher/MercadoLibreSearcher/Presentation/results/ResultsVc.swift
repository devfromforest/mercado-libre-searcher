//
//  ResultsVc.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class ResultsVc: BaseViewController {
	
	weak var coordinator: SearchCoordinator?
	
	private let resultsView: ResultsView = {
		let view = ResultsView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	private var searchViewModel: SearchProductsViewModel!
	private var dataSource: ProductsSource<Array<Product>, ProductCell>?
	var textToSearch: String = ""
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		initViewModels()
		
		searchViewModel.request = textToSearch
		searchViewModel.initObserver()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	func initViewModels(){
		searchViewModel = SearchProductsViewModel()
		searchViewModel.response(responseHandler: searchResponseCompletion,
								 errorHandler: searchErrorCompletion)
	}
	
	func setupViews(){
		resultsView.bindConstraints(to: self.view)
		loadingView.bindContraints(to: self.view)
		errorView.bindContraints(to: view)
		hideErrorView()
		showLoading()
	}
	
	private func searchResponseCompletion(data: Results?){
		fillList(productsFounds: data!.results)
	}
	
	private func searchErrorCompletion(error: NetworkError){
		hideLoading()
		errorViews(error: error)
	}
	
	private func errorViews(error: NetworkError){
		switch error {
		case .apiFailure:
			showErrorView(message: Strings.error_occurred.localized)
			break
		case .noInternet:
			showErrorView(message: Strings.not_internet_connection.localized)
			break
		case .invalidResponse:
			showErrorView(message: Strings.error_occurred.localized)
			break
		case .decodingError:
			showErrorView(message: Strings.error_occurred.localized)
			break
		}
	}
	
	private func fillList(productsFounds: [Product]){
		if dataSource == nil {
			dataSource = ProductsSource()
			dataSource?.delegate = self
		}
		
		dataSource?.data = productsFounds
		
		resultsView.populateProductList(data: dataSource!)
		resultsView.setResultsQuantityFound(quantity: productsFounds.count)
		if productsFounds.count == 0 {
			showErrorView(message: Strings.results_not_found.localized)
		}
		hideLoading()
	}
	
}
