//
//  ResultsView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class ResultsView: UIView{
	
	private var resultsLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.font = UIFont(name: Fonts.bold_font, size: 18.0)
		lbl.textColor = .darkText
		lbl.textAlignment = .left
		lbl.numberOfLines = 1
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()
	
	private var productList: UITableView = {
		let tableView = UITableView(frame: .zero)
		tableView.backgroundColor = .clear
		tableView.translatesAutoresizingMaskIntoConstraints = false
		tableView.rowHeight = UITableView.automaticDimension
		tableView.estimatedRowHeight = 200
		tableView.separatorStyle = .none
		tableView.bounces = true
		return tableView
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func registerCells(){
		productList.register(ProductCell.self, forCellReuseIdentifier: "ProductCell")
	}
	
	func setupViews() {
		registerCells()
		backgroundColor = .white
		addSubview(resultsLbl)
		addSubview(productList)
	}
	
	func setConstraints() {
		let safeArea = self.safeAreaLayoutGuide
		NSLayoutConstraint.activate([
			resultsLbl.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 8),
			resultsLbl.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 8),
			resultsLbl.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -8)
		])
		
		NSLayoutConstraint.activate([
			productList.topAnchor.constraint(equalTo: resultsLbl.bottomAnchor, constant: 10),
			productList.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 8),
			productList.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -8),
			productList.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -8)
		])
	}
	
	func bindConstraints(to parentView: UIView){
		parentView.addSubview(self)
		NSLayoutConstraint.activate([
			self.topAnchor.constraint(equalTo: parentView.topAnchor),
			self.leftAnchor.constraint(equalTo: parentView.leftAnchor),
			self.rightAnchor.constraint(equalTo: parentView.rightAnchor),
			self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor)
		])
	}
	
	func setResultsQuantityFound(quantity: Int){
		let resultLabel = Strings.results_title.localized
		resultsLbl.text = "\(quantity) \(resultLabel)"
	}
	
	func populateProductList(data: ProductsSource<Array<Product>, ProductCell>){
		productList.dataSource = data
		productList.delegate = data
		productList.reloadData()
	}
}
