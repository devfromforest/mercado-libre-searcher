//
//  SplashScreenVc.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//

import UIKit

class SplashScreenVc: BaseViewController {
	
	private var splashView: SplashScreenView = {
		let splashView = SplashScreenView(frame: .zero)
		splashView.translatesAutoresizingMaskIntoConstraints = false
		return splashView
	}()
		
	var coordinator: MainCoordinator?
	var searchCoordinator: SearchCoordinator?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(true, animated: animated)
	}
	
	private func setupViews(){
		splashView.bindConstraints(to: self.view)
		showSearchScreen()
	}
	
	func showSearchScreen(){
		DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) { [self] in
			coordinator?.showSearch()
		}
	}
	
}
