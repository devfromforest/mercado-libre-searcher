//
//  SearchScreenView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class SearchScreenView: UIView {
	
	private let LOGO_SIZE_HEIGHT: CGFloat = 128
	private let LOGO_SIZE_WIDTH: CGFloat = 167
	private var imageHelper: ImageHelper!
	private var colorHelper: ColorHelper!
	
	private var topCtn: NSLayoutConstraint!
	
	private lazy var logoImg: UIImageView = {
		let img = UIImageView(frame: .zero)
		img.image = imageHelper.showImage(named: "logo")
		img.translatesAutoresizingMaskIntoConstraints = false
		return img
	}()
	
	private var searchBarView: UIView = {
		let view = UIView()
		view.backgroundColor = .clear
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		imageHelper = ImageHelper()
		colorHelper = ColorHelper()
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setupViews() {
		backgroundColor = colorHelper.getColor(color: .primary)
		addSubview(logoImg)
		addSubview(searchBarView)
	}
	
	func setConstraints() {
		let safeArea = self.safeAreaLayoutGuide
		
		topCtn = logoImg.topAnchor.constraint(equalTo: self.topAnchor, constant: 200)
		topCtn.isActive = true
		
		NSLayoutConstraint.activate([
			logoImg.centerXAnchor.constraint(equalTo: safeArea.centerXAnchor),
			logoImg.heightAnchor.constraint(equalToConstant: LOGO_SIZE_HEIGHT),
			logoImg.widthAnchor.constraint(equalToConstant: LOGO_SIZE_WIDTH)
		])
		
		NSLayoutConstraint.activate([
			searchBarView.topAnchor.constraint(equalTo: logoImg.bottomAnchor, constant: 10),
			searchBarView.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor),
			searchBarView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor),
			searchBarView.heightAnchor.constraint(equalToConstant: 50)
		])
	}
	
	func bindConstraints(to parentView: UIView){
		parentView.addSubview(self)
		NSLayoutConstraint.activate([
			self.topAnchor.constraint(equalTo: parentView.topAnchor),
			self.leftAnchor.constraint(equalTo: parentView.leftAnchor),
			self.rightAnchor.constraint(equalTo: parentView.rightAnchor),
			self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor)
		])
	}
	
	func setupSearchBarView(searchBar: UISearchBar){
		searchBarView.addSubview(searchBar)
	}
	
	func searchBarToTop(){
		UIView.animate(withDuration: 0.3) { [self] in
			topCtn.constant = 50
			self.setNeedsLayout()
			self.layoutIfNeeded()
		}
	}
	
	func searchBarToStartPosition(){
		UIView.animate(withDuration: 0.3) { [self] in
			topCtn.constant = 200
			self.setNeedsLayout()
			self.layoutIfNeeded()
		}
	}
	
}
