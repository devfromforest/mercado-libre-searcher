//
//  ResultSearchVc.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class RecentlySearchedVc: UIViewController {
	
	private let recentView: RecentlyView = {
		let view = RecentlyView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
    }
	
	func setupViews(){
		view.backgroundColor = UIColor(named: "default_yellow")
		recentView.bindConstraints(to: self.view)
	}
}
