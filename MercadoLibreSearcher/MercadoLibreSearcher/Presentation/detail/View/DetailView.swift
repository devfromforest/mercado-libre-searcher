//
//  DetailView.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 12/09/21.
//

import UIKit
import Alamofire
import AlamofireImage

class DetailView: UIView {
	
	private let productImage: UIImageView = {
		let image = UIImageView()
		image.translatesAutoresizingMaskIntoConstraints = false
		image.image = UIImage(named: "placeholder_image")
		return image
	}()
	
	private let productNameLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.font = UIFont(name: Fonts.regular_font, size: 18.0)
		lbl.textColor = .darkText
		lbl.textAlignment = .left
		lbl.numberOfLines = 0
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()
	
	private let productPriceLbl: UILabel = {
		let lbl = UILabel(frame: .zero)
		lbl.text = ""
		lbl.font = UIFont(name: Fonts.regular_font, size: 18.0)
		lbl.textColor = .black
		lbl.textAlignment = .left
		lbl.numberOfLines = 1
		lbl.translatesAutoresizingMaskIntoConstraints = false
		return lbl
	}()

	private lazy var mainView: UIView = {
		let view = UIView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(productImage)
		view.addSubview(productNameLbl)
		view.addSubview(productPriceLbl)
		view.cornerRadius(8)
		view.setBorder(with: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
		return view
	}()
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupViews()
		setConstraints()
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	private func setupViews(){
		addSubview(mainView)
	}
	
	private func setConstraints(){
		let safeArea = self.safeAreaLayoutGuide
		NSLayoutConstraint.activate([
			mainView.topAnchor.constraint(equalTo:  safeArea.topAnchor, constant: 10),
			mainView.leadingAnchor.constraint(equalTo:  safeArea.leadingAnchor, constant: 10),
			mainView.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -10)
		])
		
		NSLayoutConstraint.activate([
			productImage.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 6),
			productImage.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 6),
			productImage.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -6),
			productImage.widthAnchor.constraint(equalToConstant: 200),
			productImage.heightAnchor.constraint(equalToConstant: 200)
		])
		
		NSLayoutConstraint.activate([
			productNameLbl.topAnchor.constraint(equalTo: mainView.topAnchor, constant: 6),
			productNameLbl.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: 6),
			productNameLbl.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -6)
		])
		
		NSLayoutConstraint.activate([
			productPriceLbl.topAnchor.constraint(equalTo: productNameLbl.bottomAnchor, constant: 6),
			productPriceLbl.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: 6),
			productPriceLbl.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: 6),
			productPriceLbl.bottomAnchor.constraint(equalTo: mainView.bottomAnchor, constant: -6)
		])
	}
	
	func bindConstraints(to parentView: UIView){
		parentView.addSubview(self)
		NSLayoutConstraint.activate([
			self.topAnchor.constraint(equalTo: parentView.topAnchor),
			self.leftAnchor.constraint(equalTo: parentView.leftAnchor),
			self.rightAnchor.constraint(equalTo: parentView.rightAnchor),
			self.bottomAnchor.constraint(equalTo: parentView.bottomAnchor)
		])
	}
	
	func setData(data: Product){
		productNameLbl.text = data.title
		productPriceLbl.text = data.price

		ImageManager()
			.thumbnailID(id: data.thumbnail)
			.downloadImage { [self] image in
			productImage.image = image
		}
	}
}
