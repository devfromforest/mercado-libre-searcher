//
//  DetailVc.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 9/09/21.
//

import UIKit

class DetailVc: BaseViewController {

	let detailView: DetailView = {
		let view = DetailView(frame: .zero)
		view.translatesAutoresizingMaskIntoConstraints = false
		return view
	}()
	
	weak var coordinator: SearchCoordinator?
	var product: Product!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		setupViews()
		self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		navigationController?.setNavigationBarHidden(false, animated: animated)
	}
	
	private func setupViews(){
		detailView.bindConstraints(to: self.view)
		detailView.setData(data: product)
	}
	
}
