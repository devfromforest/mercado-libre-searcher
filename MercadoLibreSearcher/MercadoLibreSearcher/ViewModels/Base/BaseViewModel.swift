//
//  BaseViewModel.swift
//  MercadoLibreSearcher
//
//  Created by Miller Mosquera on 7/09/21.
//

import RxSwift

class BaseViewModel<R, T> {
	
	private var subject = PublishSubject<T>()
	private var _request: R? = nil
	private let disposeBag = DisposeBag()
	
	typealias responseCompletion = (_ data: T?) -> Void
	typealias errorCompletion = (_ error: NetworkError) -> Void
	
	var request: R? {
		get{
			return _request
		}
		set{
			_request = newValue
		}
	}
	
	func initObserver(){}
	
	func responseHandler(data: T?){
		send(data: data!)
	}
	func errorHandler(error: NetworkError){
		send(error: error)
	}
	
	func send(data: T){
		subject.onNext(data)
	}
	
	func send(error: NetworkError){
		subject.onError(error)
	}
	
	func response(responseHandler: @escaping(responseCompletion),
				  errorHandler: @escaping(errorCompletion)){
		subject
			.observe(on: MainScheduler.asyncInstance)
			.subscribe { data in
				responseHandler(data)
			} onError: { error in
				/*let nsError = error as NSError
				var type: ErrorType!
				
				if nsError.code == ErrorType.InternetConnection.rawValue {
					type = .InternetConnection
				} else if nsError.code == ErrorType.RequestError.rawValue {
					type = .RequestError
				} else if nsError.code == ErrorType.RequestTimeOut.rawValue {
					type = .RequestTimeOut
				}*/
				
				errorHandler(error as! NetworkError)
			}.disposed(by: disposeBag)
	}
}
